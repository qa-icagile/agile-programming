# Test coverage exercise

* Work in pairs.

Open solution in Test Coverage\Starter.

In Visual Studio 2019 only the Enterprise edition has Coverage anlysis built in. That means you will have to install a coverage analyzer.

## Install Coverage Extension

1. Goto Extensions menu -> Manage Extensions
2. Under Online -> Visual Studio Marketplace search for dotcover
3. Install the JetBrains tool dotCover
4. Restart Visual Studio after Install
5. You may need to "Start Trial" for dot Cover tool in the Extensions menu
7. Right Click on Pie Chart icon next to Calculator class and choose to **Cover All**
8. You should see a Test Runner which should display the Test Coverage on completion

## To Do

* Determine the percentage of code currently under test in your Calculator project.
* Identify any calculator business logic not covered by test and add 1 or more tests improve the percentage


 
