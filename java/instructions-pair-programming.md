# Pair programming exercise

## Task 1

Create a class file in java to represent a **Bank Account** class.
It should include fields such as: 
* Customer name
* Account Number
* Balance
and methods such as:
* Deposit
* Withdraw

The class should represent a current/checking account and can therefore go overdrawn.

Your class should compile. There is no need to write any client code.

## Task 2

Create a class file in java to represent a Bank Account class.
It should include fields such as:
* Customer name
* Account Number
* Balance
and methods such as:
* Deposit
* Withdraw

The class should represent a savings account and therefore, may not go overdrawn. However, interest may be added.

Your class should compile. There is no need to write any client code.

## Task 3
Create two classes: Customer, BankAccount.

Each class should have a single responsibility. 

The bank account class should represent a savings account.