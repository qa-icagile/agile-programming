<a href="#"><img src="../../Documents/images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../Documents/images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../Documents/images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_yellow.png" width="100" /></a>  

<a href="#"><img src="../../Documents/images/logos/java-4-logo-png-transparent.png" width="75" /></a>  

# Java - Readable tests exercise

Work in pairs.

Change your workspace to ReadableTests using File-> Switch Workspace in Eclipse

For this exercise you will be working on the Calculator project located in the 
Readable Tests folder.

If using version control name your project calculator_readable_tests

## Task

Identify all tests that have poor names. That could be most of them.

Agree on a naming convention and then rename all of the poorly named tests.

You may consider using common design steps for unit testing as follows 

1. Setting up fixtures (creating objects) Ex: `setUp()` with `@Before`
2. Writing assertions (one assert per test case method)
3. Writing cleanup method (`tearDown()` with `@After` annotation)

