<a href="#"><img src="../../Documents/images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../Documents/images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../Documents/images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_green.png" width="100" /></a>  

<a href="#"><img src="../../Documents/images/logos/java-4-logo-png-transparent.png" width="75" /></a>  

# Java - Test coverage exercise

Work in pairs.

Change your workspace to TestCoverage using File-> Switch Workspace in Eclipse

For this exercise you will be working on the Calculator project located in the 
Test Coverage folder.

If using version control name your project calculator_test_coverage

Determine the percentage of code currently under test in your Calculator project.

Identify any calculator business logic not covered by test and add 1 or more tests 
improve the percentage

Hint: You may use Eclemma (Emma) code coverage tool for Eclipse

 
