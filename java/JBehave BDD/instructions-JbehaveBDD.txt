instructions-JBehave-BDD.txt

BDD exercise using JBehave

Work in pairs.

For this exercise you will be working on JBehaveTest project found in the JBehave TDD folder.

Copy the JBehaveTest project into the WorkingRepos folder and add the project to your IDE - remember to commit and push so the other partner can access the code on their machine.

Make sure that the existing scenario tests successfully.

Add a story to subtract one number from another and a story to multiply 2 numbers.

Eg: 

Given a calculator
When I subtract 4 from 5
Then the outcome should be 1

You will be required to update your step and core files in order to pass this test

Remember to keep your repository updated!

