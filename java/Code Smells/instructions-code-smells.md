# Instructions-code-smells.txt

## Code smells exercise

* Work in pairs.

If you already have the Code Smells project in Version control, use that code.

Otherwise use the project that can be located in Code Smells folder

* Identify flaws in an existing class Account

* Use SOLID principles to identify code smells in Account class

* Apply refactoring techniques as possible

### Remember to keep your repository updated!
