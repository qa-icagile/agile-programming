Feature: Calculate Grocery Bill

Scenario: Purchase a cake
Given the price of the "Cake" is #1.75
When I purchased 2 "cakes"
Then the bill total should #3.50

Scenario: Purchase another cake
Given the price of the "Cake" is #1.75
When I purchased 3 "cakes"
Then the bill total should #6.25

Scenario: Purchase another cake
Given the price of the "Cake" is #1.75
When I purchased 4 "cakes"
Then the bill total should #8.00