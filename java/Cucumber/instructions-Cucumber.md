# Cucumber exercise 

* Work in pairs.

For this exercise you will be working in the bank folder.

If required create a project in Git repository to store the code


* At the command-line place yourself in the Cucumber/bank folder and run cucumber.bat.

This will execute a test based on a single feature scenario.

* Edit the feature file to add a scenario for a withdrawal.

Eg:

`Scenario: Customer withdraws cash within balance
Given the customer has a balance of 500
When the customer withdraws 200
Then the balance is 300 `

* Update the step and implementation files as required to pass the new test

* Remember to keep your repository updated!

