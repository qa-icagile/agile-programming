Narrative: 
Various senarios to test Calculator

Scenario: Test add method
Given a calculator
When I add 2 and 9
Then the outcome should be 11


Scenario: Test subtract method
Given a calculator
When I subtract 4 from 5
Then the outcome should be 1

Scenario: Test multiply method
Given a calculator
When I multiply 6 and 7
Then the outcome should be 42
