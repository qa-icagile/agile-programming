package step_definitions;

import cucumber.api.java.en.*;
import cucumber.api.PendingException;
import implementation.Account;
import static org.junit.Assert.*;

public class AccountStep{
	Account acc;
	@When("^the customer withdraws (\\d+)$")
	public void whenTheCustomerWithdraws(int amount){
		 //TODO
		acc.Withdraw(amount);
	}
	@When("^the customer deposits (\\d+)$")
	public void whenTheCustomerDeposits(int amount){
		 //TODO
		acc.Deposit(amount);
		
	}

	@Then("^the balance is (\\d+)$")
	public void thenTheBalanceIs(int amount) {
		 //TODO
		org.junit.Assert.assertEquals(amount, acc.getBalance());
				
	}
	@Given("^the customer has a balance of (\\d+)$")
	public void givenTheCustomerHasABalanceOf(int bal) throws Exception{
		acc=new Account(bal);
		org.junit.Assert.assertEquals(bal, acc.getBalance());
		
	}
	/*
	@Given("^the customer has a balance of (\\d+)$", (Integer arg1) -> {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	});

	When("^the customer withdraws (\\d+)$", (Integer arg1) -> {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	});*/
}