Feature: BankAccount
Scenario: Customer withdraws cash within balance
Given the customer has a balance of 500
When the customer withdraws 200
Then the balance is 300