/***
 * Excerpted from "The Cucumber for Java Book",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material,
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose.
 * Visit http://www.pragmaticprogrammer.com/titles/srjcuc for more book information.
***/
package step_definitions;

import cucumber.api.java.en.*;
import cucumber.api.PendingException;
import implementation.Checkout;
import static org.junit.Assert.*;

public class CalculateGroceryBillSteps
{
	double	itemPrice;
	Checkout checkout;

	@Given("^the price of the \"([^\"]*)\" is \\#(\\d+)\\.(\\d+)$")
    public void thePriceOfTheIs(String item, Integer pounds, Integer pence) throws Throwable
    {
		itemPrice = pounds + (pence / 100.00);
	}

	@When("^I purchased (\\d+) \"([^\"]*)\"$")
    public void iPurchased(Integer itemCount, String itemName) throws Throwable
    {
        checkout = new Checkout();
        checkout.add(itemCount, itemPrice);
    }

	@Then("^the bill total should #(\\d+)\\.(\\d+)$")
    public void theBillTotalShould(int pounds, int pence) throws Throwable
    {
		double total = pounds + (pence / 100.00);
        assertEquals(total, checkout.total(), 0.0);
    }
}