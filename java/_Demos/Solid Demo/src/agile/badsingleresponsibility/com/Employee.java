package agile.badsingleresponsibility.com;

import java.util.Date;


// Breaks Single responsibility
// Should the Employee class have the responsibility of working out if promotion due this year?
// Also, should it calculate the income tax for the current year?
public class Employee {
	// we would have getters/setters for these
	private String empId;
	private String firstName, lastName;
	private String address;
	private Date dateOfJoining;
	private String jobTitle;

	public boolean isPromotionDueThisYear() {
		// logic to do promotion
		return true;
	}
	public Double calcIncomeTaxForCurrentYear() {
		// logic to do income tax
		return 0.0;
	}
}

