<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Delivery/NewQAIcons_Delivery_pink.png" width="100" /></a>

# Speed of Builds

## Learning objectives

* Design and implement a rapid build pipeline.
* Practice and apply techniques to improve slow tests.
* Practice and apply using build pipelines, separating slow from fast tests. 

---

# Speed of Builds

We need feedback quickly

Slow tests mean it is going to take longer to get results

What is a slow test?

Michael Feathers in his book ‘Working effectively with Legacy Code’ quotes:

>“A unit test that takes 1/10th of a second to run is a slow unit test”

---

# How do we identify slow tests?

- We need to find which tests take the most time and see how they can be optimized
- Time your tests, could be that it is the last 10% that take all the time
- Run your code through a profiler and then take note where the time is being spent

---

# How can we reduce time to test?

- Reusing data between tests
- Use a mocking framework to avoid hitting databases
- Try doing similar set up for lots of individual tests in the test fixture setup
- Run tests in parallel
- Refactor!
- Get something else to run the test for you using a continuous integration tool such as Jenkins
- Run slow tests (e.g. integration) periodically. 

---

# JUnit Extensions

## Performance tests
- Beware non-deterministic

```java
long start = System.currentTimeMillis();
long expected = 3000L;
… (perform test)
long time = System.currentTimeMillis() - start;
assertTrue("Actual time: " + time 
         + ". Target time: " + expected, time <= expected);
```

**Better:** Use JUnit extension – *JUnitPerf*
- Uses decorator pattern
- Keeps timing code separate from testing

You can use JUnit for performance testing units, but beware that such tests are non-deterministic.  The performance of a DOM parser, for instance, is highly dependant on the amount of memory on the machine it is running on.  So these tests are most useful when making comparisons on the same development machine.  Even then, they can be affected by what software is running on that machine from one test to the next.  

A **JUnitPerf** test is a decorator that wraps an ordinary JUnit test.  In this example, there is a JUnit class `DOMTraxTransTest`, which tests a an XML DOM transformation using the Crimson parser: 

```java
import junit.framework.Test;
import com.clarkware.junitperf.TimedTest;

public class DOMTraxPerfTest {
    public static Test suite() {
        long maxElapsedTime = 3000;
        DOMTraxTransTest testCase = 
             new DOMTraxTransTest("testCrimsonDOMTransform");
        Test timedTest = new TimedTest(testCase, maxElapsedTime);
        return timedTest;
    }
}
```
---

