<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Speed of Builds

## General

---

## Connections Speed of Builds

**Question:** What might be a factor in a test running slowly? (PollEv)
**Question:** What could you do to speed up a slow test? (PollEv)
**Question:** What could be done to speed up the build pipeline when slow tests can't be removed or made quicker? (PollEv)

Might a better question deal with grouping tests to isolate the slow tests and run them less frequently?

---

## Concepts Speed of Builds

Acknowledge all valid points and point out any that they might have missed. Would expect to hear parallel tests, test suits, build pipelines

Example:
    SureFile Maven plugin for running fast and slow running test separately

**Demo.** We have two test files named FAST____, and SLOW____ Configure Maven and SureFile to just run FAST

---

## Concrete Practice Speed of Builds

Students should determine the percentage of code currently under test in their Calculator project.

Students should identify what tests are necessary for a given set of code.

Identify fast and slow running tests then separate these tests.

They will demonstrate *SureFire Maven Plugin* (for Java only) for create build pipeline and configure fast and slow running tests

Notes: No exercises in anything other than **Java** at the moment

Instructions are contained in the file Java07SpeedOfBuilds.md in the \Guides folder extracted from their student files.

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.