<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_yellow.png" width="100" /></a> 

# Unit Testing Exercise

Work in pairs, complete the following in the language that the Calculator was developed in.

If you have no Calculator, a starter project has been included in the Unit Test folder for each language.

Locate the **Guides** folder for your language and then the *Unit Testing* file.

Follow the instructions to help you write some unit tests.

*Whole Activity Time Box:* **20 minutes**