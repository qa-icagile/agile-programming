<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_yellow.png" width="100"/></a>

# Unit Testing

## When would you use each test strategy and which tools/frameworks might be useful?

### When would you use each test strategy and which tools/frameworks might be useful? Are there others?

| Test Strategy      | When to use | Tools/Frameworks |
| ------------------ | ----------- | ---------------- |
| Unit Testing       |             |                  |
| Component Testing  |             |                  |
| Acceptance Testing |             |                  |
|                    |             |                  |
|                    |             |                  |

Fill in the table in your Workbook.

*Whole Activity Time Box:* **3 minutes**
