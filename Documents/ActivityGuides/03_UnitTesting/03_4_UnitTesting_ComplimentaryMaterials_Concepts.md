<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Delivery/NewQAIcons_Delivery_yellow.png" width="100" /></a>

# Unit Testing

## Learning objectives

* Build and execute automated tests using fresh and shared fixtures in program code.
* Prepare an automation strategy and explain which types of tests are used and the criteria to apply in the choice of tools.
* Apply fresh and shared fixtures in code.

---

# What are Unit Tests

A software testing method written in code where individual sections of code are tested.

We refer to a unit as the smallest testable part of the software under test.

Allows us to test bits of code in isolation.

Written before the code to be tested (known as TDD).

They are automated.

Tiny pieces of code that developers write, execute and maintain.

---

# Unit Testing vs Component Testing

## Unit Testing

* Ensures all of the features within the Unit (class) are correct
* Dependent/Interfacing units are typically replaced by stubs, simulators or trusted components
* Tools that allow component mocking/simulation are often used

## Component Testing

* Similar to Unit testing but with a higher level of integration between units
* Units within component are tested as together real objects
* Dependent components can be mocked

## Integration Testing 
* Involves the testing of two or more integrated components

---

# Some benefits of Unit Tests

As they test our code, shows us if our code really works

Saves a lot of time later down the line
  * Helps prevent code later down the line not function how it should

Deployment is a lot easier
  * We can use continuous integration (CI) to run our tests, pull down latest changes, push code to a remote repository

Detects defects early on – software bugs found early

Improves the quality of our code

Simplifies the debugging process

Localise the source of the bug more easily

---

# Unit Testing with JUnit

![JUnit](../../images/unit-testing-junit.png)

## Objectives
* What are manual and unit tests
* To review JUnit for developers 
* Build and execute automated tests
## Contents
* Unit test overview
* JUnit overview
* The TestCase and Assert classes
* JUnit annotations
* Unit testing best practice
* Hamcrest matchers
* Exception testing
* Testing Patterns

---

# Automated Testing

![Manual Testing](../../images/unit-testing-manual.png)
### Manual Testing
* Difficult to repeat tests in a consistent manner
* Can’t guarantee that in regression testing same values are re-entered
* In speed tests, it’s difficult for an operator to match a computer
* Can only be executed by certain people

![Automated Testing](../../images/unit-testing-automated.png)
### Automated Testing
* Can be executed by anyone
* Perfect for regression testing
* Series of contiguous testing can be done, where the results of one test rely on another
* The build test cycle is increased

---

# Manual Testing

Write a test harness for the Class Under Test 
* Main method creates instance of class, invokes method, System.out.println()s 

### Drawbacks 
Manual inspection to see if code performed correctly
* error prone
* not scalable 
Do not aggregate (x out y tests passed)
Do not indicate how much of the code was exercised
Do not integrate with other tools – build process, CI<br /> 
`main()` does not tell you what the scenario is
Not extensible

----

# Unit Testing

A unit can be
* A method
* A database query, stored proc or transaction
* A dynamic web page

A unit test “tests one behaviour that is expected from an object.  It is also automated, self-validating, consistent/repeatable, independent, readable, easy to maintain and fast.”

### The xUnit Framework
Common design: setup, test, assertion; suites of tests
Original: SUnit for Smalltalk (Kent Beck)
JUnit and TestNG for Java, NUnit for .Net, Test::Unit for Perl, DbUNit – extension of JUnit for databases, FlexUnit for Flex, etc

Unit Testing is also known as Component or Module Testing.  The concept originated in the 1970s, and the initial concept of what constitutes a “unit” was probably quite open-ended. It is a form of "White Box" testing – in other words, where the test has complete knowledge of the internals of the component being tested.  This contrasts with "Black Box" testing, in which only the interface of the component is being tested.

*Robert Martin (Clean Code, Ch. 9)* captures the modern conception of unit tests in the acronym FIRST:
Fast: they need to be fast so that developers will run them very frequently
Independent: one test must not be dependent on, or be affected by, any other test
Repeatable: they must be repeatable in every environment: development, QA, production
Self-Validating: they should either pass or fail automatically, not require any manual intervention or manual inspection of output logs
Timely: they should be written in a timely fashion (just before the application code – i.e. TDD)

For a list of xUnit implementations, see
http://en.wikipedia.org/wiki/List_of_unit_testing_frameworks

---

# Unit Testing

Core practice of XP

Can be adopted within other methodologies

*"Test-first programming is the least controversial and most widely adopted part of Extreme Programming (XP). By now the majority of professional Java™ programmers have probably caught the testing bug"* – **Elliotte Rusty Harold**

Test written before implementation
* Tools and techniques make TDD very rigorous process

AKA Test Driven Design
* Tests drive design of API

*It is inconceivable that in today’s world any serious developer would develop an application without using a modern IDE with powerful refactoring capabilities, and develop their code in a largely test-driven manner.  The risks of not driving development through tests are too great.*  

*When you’ve done TDD, you will be familiar with this experience: “I’ll just make this little change, I know it’s not going to have any consequences”… and then you re-run the tests, only to find that some tests are now red.  “Oh yes, of course… that change led to this consequence, which led to that now being wrong.”*  

*If you hadn’t got the suite of tests covering your code base, that little bug would have insinuated itself into your application, only to come to light some months later, requiring someone to spend a lot of time tracking it down, with who knows how many other repercussions for other bits of the code.* 

*So, when you become ‘test infected’, when you have some new bit of functionality to implement, you don’t just dive in and start coding away.  You sit back and ask yourself: “So, how am I going to specify my next step in a test?”  And sometimes you may find yourself not being ‘productive’ in a conventional sense, thinking about your tests, discussing them with your colleagues, reorganising your tests, breaking large test classes up, extracting common setup to superclasses etc.  And then suddenly you may be able to write a whole bunch of new tests, and then surprisingly quickly implement the code which they specify.*

---

# Coding Tests by intention

Tests should be coded consistently with a well defined clear intent

Four phases are standard
* Setup
  - Setup the initial state for the test
* Exercise		
  - Perform the action under test
* Verify 
  - Determine and verify the outcome
* Cleanup 
  - Clean-up the state created

Each phase should be 
* Clearly Expressed
* Well documented

Expected Outcomes should be clearly expressed

---

# Unit Testing

## Test – Code – Refactor

### Kent Beck's summary of TDD:
1. Write new code only if you first have a failing automated test
2. Eliminate duplication

### Red – Green – Refactor 

![Red-Green-Refactor](../../images/unit-testing-rgr.png)

The graphic on the left illustrates that the famous green bar is indeed a progress bar. The image on the right shows the final outcome of running this series of tests: there are 3 failures.  

---

#  Simple JUnit example

## Class Under Test (CUT):

```java
public class Example {
    public boolean canJoin(int age) {
        return age > 21;
    }
}
```

## JUnit:

```java
class ExampleTests {
    private Example cut;

    @BeforeEach
    void setUp() throws Exception {
        cut = new Example();
    }

    @Test
    void testCanJoin() {
        int age = 15;
        boolean expected = false;
        boolean result = cut.canJoin(age);
        assertFalse(result);
    }
}
```
---

# Another JUnit example

## Class Under Test (CUT):

```java
public int sumArrayOfIntergers(int[] array) {
    int sum = 0;
    for(int i = 0; i < array.length; i++) {
        sum += array[i];
    }
    return sum
}
```

## JUnit:

```java
@Test
void testSumArrayOfIntegers() {
    int[] numbers = {10, 20, 30, 40};
    int expected = 100;
    int actual = cut.sumArrayOfIntegers(numbers);
    assertEquals(expected, actual);
}
```

---

# JUnit: Principal Java xUnit framework

Developed by:
* Kent Beck (Extreme Programming – XP)
* Eric Gamma (Design Patterns)

3 Versions
* JUnit 3 (main package: junit.framework)
* JUnit 4 (main package: org.junit)
* JUnit 5 (result of JUnit Lambda, focuses on Java 8 and above)

How to run
* Command line – central to build scripts
* `java org.junit.runner.JUnitCore my.pkg.AllTests`
* IDE: Eclipse, IntelliJ, NetBeans
* Standalone GUI: AWT, Swing – JUnit 3 only

---

# junit.framework.Assert 

TestCase inherits from Assert

#### Methods are overloaded – e.g.<br />
`assertEquals(boolean expected, boolean actual)`<br />
`assertEquals(Object expected, Object actual)`<br />
`assertEquals(String message, Object expected, Object actual)`<br />
* Use String version: on failure, exception thrown with message
* Remember order: expected then actual

#### Paired methods<br />
 `assertSame()`/`assertNotSame()` – identity of reference<br/>
 `assertTrue()`/`assertFalse()` – String message, boolean condition<br/>
`assertNull()`/`assertNotNull()` - String message, Object obj<br/>

`fail(String message)`

`assertEquals()` is overloaded for all the primitives, and `Object`, and `String` – so 10 versions – then double that for the three argument version which starts with a `String` message.  For the two items compared, always give the expected one first, then the computed one, for this is the order *JUnit* will use in error reporting (as in `"java.lang.AssertionError: expected:<7.0> but was:<8.0>"`).  

Always use the `String` version of an `assert()` method so that the reason for failure will be stated if the assert fails.

---

# Example – class under test 

```java
public class Person implements Comparable<Person>  {
	private String givenName;
	private String familyName;
	private int age;
	 // 3 arg constructor, getters and setters, etc. 

	@Override public int compareTo(Person other) {
		int otherAge = other.age;
		return this.age - otherAge;
	}
   @Override public String toString() {
        return familyName + ", " + givenName + " [" + age + "]";
   }	
}
```

---

# In Eclipse you can...

Generate test stub from CUT:
* Right click `CUT` -> `New` -> `JUnit Test Case`

1st dialog: which general JUnit  methods do you want?
* `setUp()` usually sufficient

2nd dialog: which methods in CUT (or general `java.lang.Object` methods)  do you want to generate tests for? 
* For `compareTo()`, `testCompareTo()` will be generated
Method body: `fail("Not yet implemented");`<br/> 

Generate CUT from test class when strictly following TDD

Generate blank test class from 1st dialog only

![Eclipse Test Stubs](../../images/unit-testing-eclipse1.png)

---

# Example – define your tests 

```java
import junit.framework.TestCase;
public class PersonTest3 extends TestCase {
	Person fred; 
	Person bill; 
	Person jane; 
	protected void setUp() throws Exception {
		fred = new Person("Fred", "Foggs", 29);
		bill = new Person("Bill", "Boggs", 31);
		jane = new Person("Jane", "Joggs", 29);
	}
	public void testCompareTo() {
		assertTrue("fred is 'before' bill", fred.compareTo(bill) < 0); 
		assertTrue("bill is 'after' jane", bill.compareTo(jane) > 0); 
		assertEquals("fred and jane are equivalent", 0, fred.compareTo(jane));
	}
```

Here the emphasis is on testing the method – rather than a “behaviour” – and since there are three possible return values of `compareTo()`, it could be argued that it is natural to group together the three assertions that cover these three cases.  

However, that would be wrong.  For each test method:
* There should be at least one assert (otherwise it’s not actually a test) 
* There should be at most one assert (if there is more than one assert, as soon as one fails, the remainder will not be exercised.)  

---

# JUnit Test Class (to be improved…) 

```java
import org.junit.*;
import static org.junit.Assert.*;
public class PersonTest {
	Person fred, bill, jane; 
	@Before public void setUp() throws Exception {
		fred = new Person("Fred", "Foggs", 29);
		// construct other fixtures
	}
	@Test public void testCompareTo() {
		assertEquals("fred equiv. to jane ", 0, fred.compareTo(jane));
		assertTrue("fred is 'before' bill", fred.compareTo(bill) < 0);
		assertTrue(“bill is ‘after' jane", bill.compareTo(jane) > 0);
   } 
}
```

JUnit 4 defines its own custom annotations, and we must import them to use them.  (Our test class no longer subclasses one in the JUnit API.)  It is now the annotations `@Before`, `@Test`, `@After` that mark methods as respectively setUp, test and tearDown methods, so those methods could conform to different naming conventions.  

If multiple classes have common `setUp()`/`tearDown()` code, abstract this out to a common superclass.  Don’t put any @Test methods in this superclass (else they will be inherited in each sub-class and therefore run multiple times).  The common `@Before` method will be called before any `@Before` method in a subclass.  Naturally, if the method is called both `setUp()` in sub and super class, the former will override the latter.  A class can have multiple `@Before` methods (say `setUp()` and `init()`), but apart from this case of inheritance, it would not be a good design to have two set-up methods defined in the same class.

---

# Four statuses of a test

1. **Passing:** ultimately all our tests must pass

2. **Failing:** in TDD always start with a test which fails 
3. **Erroring:** test neither passes nor fails
  - Something has gone wrong, a run-time error has occurred
  - E.g. necessary library jar has not been provided
4. **Ignored:** `@Test` `@Ignore`<br/>

![4 Test Statuses](../../images/unit-testing-4statuses.png)

---
