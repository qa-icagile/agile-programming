<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Unit Testing

## General

Introduce unit testing in context of automation.

---

## Connections - Unit Testing

TODO: Realign with Learning Module...

Ask them to spend 3 minutes describing unit testing

**Question:** What are the different types of tests? (PollEv)

**Question:** Define unit testing? (PollEv)

**Activity:** When would you use each test strategy and which tools/frameworks might be useful? Are there others?

---

## Concepts - Unit Testing

Acknowledge all valid points and point out any that they might have missed. Would expect to hear about quality, sometimes missed, code coverage.

- Black-box and White-box testing
- Unit Testing
- Acceptance Testing
- Component Testing

*Demo:* Demonstrate unit testing in your language of expertise, showing **unit tests** and **test fixtures**

Java demo uses JUnit and Hamcrest library

---

## Concrete Practice - Unit Testing

Start with Calculator project that they currently have in their repo.

Write some tests for the existing string calculator functions: Subtract, Divide and Multiply. 

**Note:** There is no Add function yet. They will write add when we cover TDD.

Write at least five tests. Including one that tests for overflow with the Multiply function.

All tests must pass.

Do not update the functions.

Students will demonstrate shared and fresh fixtures

Instructions are contained in the file [03_3_UnitTestingConcretePractice-PreferredLanguage.md](03_3_UnitTestingConcretePractice-PreferredLanguage.md).

Remind students to continue using version control (e.g. Git) for this project.

**Activity:** 5 names Multi Choice Class, Feature, Fixture

**TODO:** Need 5 made up names for test functions

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.