<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_yellow.png" width="100" /></a> 

# Test Doubles Exercise

Work in pairs, continue working on the Calculator project in your chosen language.

Locate the **Guides** folder for your language and then the *Test Doubles* file.

Follow the instructions to write find out how to identify and speed up tests.

*Whole Activity Time Box:* **20 minutes**