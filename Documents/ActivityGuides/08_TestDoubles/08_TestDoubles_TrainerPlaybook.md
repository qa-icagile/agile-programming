<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Test Doubles

## General

---

## Connections Test Doubles

**Question:** Why might you substitute a production class for one that looks similar (a double) while testing? (PollEv)

---

## Concepts Test Doubles

Acknowledge all valid points and point out any that they might have missed. Would expect to hear need of collaborators, testing external database, dependency injection and Mock libraries

Examples
- Stubs and Mocks 
- JMock, EasyMock, Mockito and PowerMock
 
---

## Concrete Practice Test Doubles

Students will create a new Java Project to demonstrate stubs and mocks 
They will complete one exercise where they will
- Write a class with a dependency on a class that does not exist yet.
- Create a new class to mimic the dependency – our “stub” class
- Perform Dependency Injection using Constructor Injection
- Create a new class to perform the dependency – our “concrete” class
  
Instructions are contained in the file <lang>08_TestDoubles.md in the <lang>\Guides folder.

NOTE: Rewrite this exercise. Provide all of the starter code for them. There is no need for them to do all of that typing. Give them the code with tests without double. Have them run the tests and time them. Have them write double(s) to improve code. Also. Make sure the starter code is more readable !!!

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.