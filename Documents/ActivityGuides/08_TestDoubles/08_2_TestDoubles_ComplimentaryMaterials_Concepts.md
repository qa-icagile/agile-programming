<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Delivery/NewQAIcons_Delivery_orange.png" width="100" /></a>

# Test Doubles

## Learning objectives

* Write code using stubs and mocks and implement dependency injection without creating complex chains of objects.
* Write code that uses stubs or mocks.
* Explain how to apply dependency injection without creating complex chains of objects.

---

# Testing Doubles Introduction

### Our code interacts with other things:
- Network resource – database, web service, etc.
- Code being developed in parallel by another person/team 
- Container (e.g. objects with lifecycle methods)

### What if the collaborator...
- ...does not exist yet
- ...is not available for use in tests
  
**One possibility:** run it in its real environment 
- Integration testing
- Important to test interactions between your class and the rest
  
**But:** you need to test the logic of your class in isolation 

>Roy Osherove: “An external dependency is an object in your system that your code under test interacts with, and over which you have no control.  (Common examples are filesystems, threads, memory, time, and so on.)”  The Art of Unit Testing, p. 50.  

Objects with which the class under test interacts are also known as its collaborators, and substitutes for them used in testing are sometimes referred to as test doubles.  Osherove uses the term fake to cover both stubs and mocks.  

---

# What is Mocking?

It is a way to test your code functionality in isolation

Does not require:
- Database connections
- File server reads
- Web services

The mock object itself does the ‘mocking’ of the real service, returning dummy data

The mock object is just simulating the behaviour of a real method, external component

---

# Stubs

## Stub: controllable replacement for existing dependency

>"Provide canned answers to calls made during the test, usually not responding at all to anything outside what's programmed in for the test ... may also record information about calls" – Fowler

A class which is (ideally) the simplest possible implementation of the logic in the real code

**Good for:** coarse-grained testing - replacing a complete external system, e.g. web server, database

**But:**
- Often complex to write
- Introduce their own debugging & maintenance issues
- Not so suitable to fine-grained unit testing

The quote from Martin Fowler's comes from his article "Mocks Aren't Stubs", which can be found at
http://martinfowler.com/articles/mocksArentStubs.html

>Imagine you want to test the logic of something like a Servlet in isolation, i.e. without placing it in a container and firing browser requests at it.  A Servlet is passed instances of the interfaces HttpServletRequest and HttpServletResponse when its service method is invoked.  On the stubbing approach, you would create your HttpServletRequestStub class, and this would involve implementing the 30 methods in the HttpServletRequest interface and the 36 from its super-type, the ServletRequest interface.  (See http://docs.oracle.com/javaee/6/api/javax/servlet/http/HttpServletRequest.html)

---

# Worked Java Example

## Task: to generate a String of six different random numbers, in format “9 – 41 – 13 – 7 – 25 – 38”

### Original version: untestable

```java
	Set<Integer> numbers = new HashSet<Integer>(LOTTERY_SIZE);
	while (numbers.size() < LOTTERY_SIZE) { 
		numbers.add(rand.nextInt(HIGHEST_NUMBER)+1);
	}
	// ...
	for (Integer number : numbers) {
		sb.append(number);
		if (++count < numbers.size()) sb.append(" - ");	
	}
```

We’re going to work with the example of simulating a lottery, because it allows us to bring out some of the core issues with mocks and stubs, without the distractions of working with an extra API.  

Our starting point is a class with a single method, `main()`.  It declares some fields, 

	```java
	private static final int LOTTERY_SIZE = 6;
	private static final int HIGHEST_NUMBER = 49; 
	private static Random rand = new Random();
	```

The `main()` method starts by exploiting the fact that a Set rejects duplicates, as a way of ensuring the 6 lottery numbers will be distinct.  The second half of `main()` is then concerned with the correct formatting of the results.  It starts with a StringBuffer and count variable (not shown above), and builds up the string with the numbers spaced apart in the format required.

## Lottery: refactoring for testability

- Break `main()` up into more testable parts
- Method `generateRandomSet()` which returns a `Set<Integer>`
- Method `formatSet()` which takes a `Set<Integer>` & returns `String`
- Formatting logic can be tested with determinate set of ints

Break out number generation into method:

```java
	static int generate(int limit) {
		return rand.nextInt(limit)+1; 
	}
```

Declare interface with this method: 

```java
	public interface NumberGenerator {
		public int generate(int limit);
       }
```

When you take TDD to heart you will naturally be writing code in small, testable units, favouring methods which return a value (which can be tested) rather than methods whose return type is void.  Here our imagined starting point was not developed that way, and we are having to manipulate it into a more testable form.  By separating out the formatting logic we can test it with a set of numbers which the test can control, e.g. 

```java
	@Test 
	public void formatIsHyphenSeparatedSequenceOfNumbers() {
		Set<Integer> numbers = new HashSet<Integer>();
		numbers.addAll(
			Arrays.asList(new Integer(20), new Integer(30), new Integer(10)));
		assertTrue(Lottery.formatNumbers(numbers).
			matches("^\\d+ - \\d+ - \\d+$"));
	}
```

## Lottery: manual stub

Lottery class can now use Constructor Injection: 

```java
	private NumberGenerator generator; 
	public Lottery(NumberGenerator generator) {
		this.generator = generator; 
	}
```

But tests need repeatability

Define a stub which provides determinacy: 

```java
	public class NumberGeneratorStub implements NumberGenerator {
		private int number = 0;
		public int generate(int limit) {
			return number++; 
		}
	}
```

The real lottery runner can inject an instance of the interface that generates real random numbers, i.e. its `main()` could look like this:

```java
		Lottery lotto = new Lottery(new RandomNumberGenerator());
		System.out.println(lotto.formatNumbers(lotto.generateRandomSet()));
```

The test method which uses this lotto is shown on the next slide.  We remove the dependency on something outside the control of the test, and achieve a test which has a guarantee of repeatability. 

Functionality of `generateRandomSet()` can be tested:

```java
	@Test
	public void formatNumbersReturnsNumbersInLotteryFormat() {
	      Set<Integer> numbers = lotto.generateRandomSet();
	      String expected = "0 - 1 - 2 - 3 - 4 - 5";
	      assertThat(lotto.formatNumbers(numbers), is(expected));
	}
```

Stub can even record interactions of CUT
- Tests can verify that CUT e.g. made expected number of calls
- Coincidentally number used for dummy data in this case is exactly that, so `verifyCallsMade()` can return it

```java
	assertThat(generatorStub.verifyCallsMade(), is(6));
```

The LotteryTest class starts like this, i.e. injecting a stub instance into the lottery: 
	
```java	
	private Lottery lotto;
	@Before
	public void setUp() {
		NumberGenerator generatorStub = new NumberGeneratorStub();
		lotto = new Lottery(generatorStub);
	}
```

Incidentally, the real random number generate method can also be tested using the Hamcrest matchers.  The test serves as a constraint, and documents the fact that, that the range is strictly greater than zero but inclusive of the upper bound:

```java
	@Test @SuppressWarnings("unchecked")
	public void generateGivenLimit10ReturnsNumberInRange() {
		NumberGenerator generator = new RandomNumberGenerator();
		for (int i = 0; i < 10; i++) {
			assertThat(generator.generate(10),
					allOf(greaterThan(0), lessThanOrEqualTo(10)));
		}
	}
```

---

# Mock objects (mocks)

>Mock: "an object created to stand in for an object that your code will be collaborating with.  Your code can call methods on the mock object, which will deliver results as set up by your tests" – Massol, p. 141

To mock a database ResultSet:

- You are not creating an object with state (records with mock data sets)
- You are creating an object which will respond to (method calls from) your code as if it had a certain state

Several Dynamic Mock Frameworks in Java:

- EasyMock
- JMock
- Mockito
- Powermock
- JMockit and more

Mocks and stubs are not necessarily mutually exclusive; it may be appropriate, depending on the class being tested, to combine use of mocks and stubs.   

Mocks and stubs can be handwritten, simple classes – if they remain simple they will have the advantage of simplicity and readability over frameworks.  A dynamic fake object is a mock or stub created at runtime – using a framework, rather than handwritten.  jMock and EasyMock are two leading Java mock objects frameworks, but there are others.  See http://mockobjects.com; also listed are mockito (http://mockito.org), rMock (http://rmock.sf.net), and SevenMock (http://seven-mock.sf.net).  

Some argue that "Isolation framework" is a better term than "Mocking framework", because it more clearly signals the intent: isolating the unit tests from their external dependencies. 

---

# Stubs vs. mocks 

Stubs enable tests by replacing external dependencies  
Asserts are against the Class Under Test, not the stub  

Test     <————>    CUT   <————>     Stub  
         asserts        interacts 


**Mocks:** can be used for stubbing (as above)

**Or:** test can ask a mock to verify that the CUT interacts with the external dependency in the correct way  
Asserts are verifies against the mock  

CUT    <————>     Mock    <————>    Test  
      interacts 	      asserts

### Mock object: 

>“a fake object … that decides whether the unit test has passed or failed.  It does so by verifying whether the object under test interacted as expected with the fake object.  There’s usually no more than one mock per test.”  (Osherove, p. 84) 

Using a mock is much like using a stub, except that the mock will record the interactions, which can be verified.  Tests should test a single thing, so there should be at most one mock per test – all other fakes should be stubs.  Having multiple mocks means you’re not just testing a single thing. 

---

# When to use mock objects 

>When the real object:
> - Has non-deterministic behaviour 
> - Is difficult to set up
> - Has behaviour that is hard to cause (such as network error) 
> - Is slow 
> - Has (or is) a UI 
> - Uses a callback (tests need to query the object, but the queries are not available in the real object (for example, “was this callback called?”) 
> - Does not yet exist  
>From: Mackinnon, Freeman and Craig “Endo-testing: Unit Testing with Mock Objects”

---

# A useful consequence of mocking

### Don’t Stub, Mock  

- Think how many times you’ve had to write stub code just get something compiled or to debug and walk through a piece of code

### Mock it, don’t stub it…

- You may not have access to some because it is being written by another team, but you do have a set of agreed interfaces
Mock those interfaces, set the desired expectations
- Debug your code

![Mocking](../../images/TestDoubles1.png)

---

# Drawbacks of mock objects

### Do not test interactions with container or between the components

- Do not give full confidence that code will run in target container
- i.e. still need integration tests

### Tests coupled very tightly to implementation

- Expectation-setting makes tests mirror internal implementation


### Do not test deployment part of components

### Most frameworks cannot mock static methods, final classes and methods

- Static methods can be mocked with workaround
- Enabled by: Powermock (extends easymock & mockito)

(The first three points here are adapted from Massol, JUnit In Action, p. 171.)

---

# Dependency Injection

Dependency is all about injecting dependencies, setting relations between instances

It helps to remove tight coupling

We use interfaces instead of using concrete classes to illustrate a dependency

```java
	public interface IEngine { }
	public class FastEngine implements IEngine { }  tight coupling
```

We ‘inject’ the interface into the class:

```java
	public class FastEngine { 
           private IEngine engine;
           public FastEngine(IEngine engine) { this.engine = engine; } 
      }
```

## Dependency Injection - Example

- Without Injection

```java
public void print() {
   Owner owner = new Owner();
   owner.setName(“Owner1”);
   owner.setId(98765);
   System.out.println(“Owner “ + owner.getId() + ” is “ + owner.getName());
}
```

- Using Injection

```java
public void print(Owner owner) {
   System.out.println(“Owner “ + owner.getId() + ” is “ + owner.getName());
}
```

## Dependency Injection Methodology

Rather than creating concrete instances, inject them at runtime

Two ways to perform Dependency Injection:
- By Constructor
- By Setter method

Need a way to manage (or wire) the dependencies
- In Java, the Spring framework provides ways to do this – it supports DI out of the box using annotations

### Constructor

```java
public class Owner
{
	Pet pet;
	
	public Owner(Pet pet) 
	{
		this.pet = pet;
	}
}
```

- The value of the Pet is set by calling a constructor method

### Setter method

```java
public class Owner
{
	Pet pet;
	
	public void setPet(Pet pet)
	{
		this.pet = pet;
	}
}
```

- The value of the Pet is set by calling a setter method

## Dependency Injection Containers

We can use a dependency injection container which can inject the mock objects into your unit tests during unit testing

Some DI containers include:
- Spring DI
- Butterfly DI Container
- Dagger
- Guice
- PicoContainer

Many unit tests don’t require a DI container if their dependencies are simple to mock out

## Dependency Injection Summary

- Always use interfaces instead of using concrete classes to illustrate a dependency
- Avoid to implicitly set the concrete implementation of a dependency in the class itself
- A dependency can be set through constructor or setter based injection
- Dependency injection makes unit testing very flexible
- Use of DI container for complex dependencies

We inject the dependency through a constructor or a setter.

References come from: https://javaranch.com/journal/200709/dependency-injection-unit-testing.html

---

# EasyMock

Using EasyMock allows us to create objects very simply

Using Java Reflection the mock object is created for a given interface

Some benefits of using EasyMock are:
- Not having to write mock objects yourself
- Exceptions are supported
- Checking order of method calls is supported
- By using annotations allows for mocks to be created even easier
- Refactoring safe allowing method names to be changed without breaking your test code

In order to work with EasyMock you will need to have the following files which also need adding to your classpath:

EasyMock: http://easymock.org/

Objenesis: http://objenesis.org/download.html

Cglib: https://github.com/cglib/cglib

EasyMock creates an object based on an interface or a class:

```java 
IBankingMethod bankMethod = createNiceMock(IBankingMethod.class);
```

EasyMock has a number of other methods which configure the mock object:
- `except()` – tells EasyMock to simulate a method with certain args
- `andReturn()` – defines the return value of this method
- `times()` – defines how often the mock object is called
- `replay()` – called to make the mock object available	

## Simple Example:

```java
	public interface IBankingMethod {
        double deposit(double cash);
		double withdraw(double cash);
    };
      
    IBankingMethod bankMethod = createNiceMock(IBankingMethod.class);
	expect(bankMethod.deposit(500).andReturn(bankMethod.Balance());
	expect(bankMethod.withdraw(100).andReturn(bankMethod.Balance());
	replay(bankMethod);
```

## EasyMock Prodcedure

- Create required mock objects
- Create instance of CUT, replacing dependencies on real collaborators with mocks
- Set expectations on mock(s)
	- Expect this method call, and fake the behaviour of the real thing by returning this particular value when that happens
- `replay()`: expectation-setting has finished
- Invoke method on CUT (as with any JUnit test)
- `verify()`: did the mock get the calls we expected?
- Optionally: make assertions (as before)

Note that all the best Java mock object frameworks are type-safe: the mock really is an instance of whatever interface it mocks.  Your favourite Eclipse keyboard shortcut,**<CTRL> <SPACEBAR>** will show you that the mock object can have just the same methods invoked on it as any other instance (of a class which is an implementation) of the interface.  

For this reason, best practice suggests you adopt a naming convention like starting all your mock object identifiers with mock, to make it clear which are the mocks and which the real objects in your code.  

## EasyMock – details

1. Create JUnit test class

2. Create mock implementations of interfaces
	- `createStrictMock()`: there must be no unexpected method calls, and the order must be as per expectations
	- `createMock()`: there must be no expected calls, but the order can be different to the order of expectations
	- `createNiceMock()`: there can be method calls from the CUT which were not expected, and the order is not checked

### The art of unit testing – finding the right balance
- Tests must not be trivial, too easy to pass
- Tests must not be too severe – aka “brittle”: fail too easily even when legitimate changes are made to the CUT
- General advice: avoid strict mocks

3. Expectations-setting phase
	- Nice mock will return null, or 0 or false for unexpected calls

Methods with void return type: don't use `expect()`:

```java
mockResponse.setContentType("text/html");
expectLastCall();
```

Throw an exception
``` java
expect(mockResponse.getWriter()).andThrow(new IOException());
```

Change the cardinality of an expectation:
- `anyTimes()`
- `atLeastOnce()`
- `times(int count)`
- `times(int min, int max)`

The return type of an `expect()` call is the interface `IExpectationSetters`, and it is here that the method `andReturn()` is found (return type also `IExpectationSetters`).

4. Replay phase

```java
replay(mockRequest, mockResponse);
```

- Finished setting expectations, ready to check if the expected method invocations are actually made
- `replay()` is a varargs method 

5. Invoke method on instance of tested class
6. Verification phase

```java
verify(mockRequest, mockResponse);
```

- Verifies that the tested class did perform all the method calls that were expected of it

- Optionally, `reset()` a mock object to re-use for a new set of expectations etc. 

Verification can be factored out into the `tearDown()` method, if appropriate: 

```java
@After
protected void tearDown() throws Exception {
	verify(mockRequest, mockResponse);
}
```

---

# Lottery - EasyMock Test

```java
@Test public void generateRandomSetReturnsLotteryNumbers() {
	NumberGenerator mockGenerator = 						createMock(NumberGenerator.class);
	for (int i = 1; i < 7; i++) {
		expect(mockGenerator.generate(anyInt())).andReturn(i);
	}
	replay(mockGenerator);
	Lottery lotto = new Lottery(mockGenerator);
	Set<Integer> numbers = lotto.generateRandomSet();
	assertThat(lotto.formatNumbers(numbers), is("1 - 2 - 3 - 4 - 5 - 6"));
	verify(mockGenerator);
}
```
This case is slightly atypical in that we need to say to the mock, in effect 

```java
	expect(mockGenerator.generate(49)).andReturn(1);
	expect(mockGenerator.generate(49)).andReturn(2);
```

etc. We need the mock to return different numbers every time `generate()` is called.  Otherwise we could have expressed that very succinctly as 

```java
	expect(mockGenerator.generate(49)).andReturn(1).times(6);
```

The for loop is there simply to set up that series of six expectations,  as the variable i increments each iteration.  

Notice that the argument matcher `anyInt()` rather than the hard-coded value `49` makes the test more flexible, without undermining its essential rigour.  Using `anyInt()` means that if the Lottery highest number was changed to a value other than `49`, these expectations would not break.  



