<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Delivery/NewQAIcons_Delivery_green.png" width="100" /></a>

# Version Control

## Learning objectives

* Apply version control using both approaches (client/server or distributed) and contrast the two approaches.

---

# Version Control

## Why use version control?

![Argh!](../../images/argh.png)

Keep track of code and changes
- One copy of the code everyone has access to
- No more mailing around code and confusion trying to integrate it
- Automated version management 

Allows for multiple people to edit a single project at the same time
- Push changes to the central repository
- Everyone can pull changes from others
- Merge together changes in files where there are conflicts

Branch code to work on specific parts
- Version 2.3 doesn’t need to die because someone else wants to look at version 3

---

# Source Control

Don’t mail around versions of your code!
- Automated version management 

Allows for multiple people to edit a single project at the same time

Push your changes to a central repository
- ONLY push changes when they work

Pull changes from other people from the central repository

Merge changes together where you’re both using the same files
- Source control works best with plain text

Branch code to work on specific parts
- Version 2.3 doesn’t need to die because someone else wants to look at version 3

---

# GIT as a DVCS

GIT’s origins are in Linux Development and is open source

Its goals were to create a DVCS system that was:
- Fast
- Simple
- Strong support for non-linear development
- Fully distributed
- Able to handle large projects like the Linux kernel efficiently

---

# Version Control Systems (VCS)

Version control is a system that records changes to files

A VCS allows you to manage file history allowing you to:
- Rolling back to previous states of a file if something goes wrong
- Maintain change logs allowing you compare versioning issues

The simplest form of version control systems are local e.g. rcs in Mac OS x

![Tradional Version Control](../../images/git1.png)

---

# Centralised Version Control Systems

Central VCS systems allow multiple developers to collaborate on other systems.
- e.g. CVS, Subersion and Perforce
Single server that contains all versioned files
- Clients that check out files from this source

![Centralised Version Control Systems](../../images/git2.png)

---

# Distributed Version Control Systems 

DVCS do not just check out the most local snapshot of a file
- They mirror the repository 
- so each checkout is like a backup of the repository

![Distributed Version Control Systems](../../images/git3.png)

---

# Git Basics

Initialising repositories
Creates a new subdirectory and creates a git repository skeleton

`git init`

Initialising a repository with existing files

`git add *.pp`

`git add README.md` 

`git commit -m "Initial commit"` 

Cloning an existing repository
- Git can use a number of protocols including http and SSH

`git clone git://github.com/resource`

---

# Recording Changes to a Repository

Each file in a  git directory can be tracked or untracked
- Tracked files are files that were in the last snapshot
  - They can be unmodified, modified or staged
- Untracked files are everything else
  - Not in your last snapshot or staging area

![Git file lifecycle](../../images/git4.png)

The main tool you use to determine which files are in which state is the git status command. If you run this command directly after a clone, you should see something like this:

`git status` 

On branch master nothing to commit, working directory clean

Note: The graphic shown above is taken from the following article:

https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository


## Staging new or "modified" files

`git add <filename>`

You can have GIT ignore files or folders through the ```.gitignore``` file
- Add them to the file
- This can be automated

---

# Working with Remote Repositories

Git projects are often held on remote repositories 
- These hold versions of a project or dependencies on the web/network

To see configured remote repositories run the `git remote` command
- If you have cloned a repository you should see the origin
- To add a repository:

`git remote add [shortname][url]`

*shortname* becomes an alias for access to the repository

### Pushing to a repo

When you have your project at a point you want to share you have to **push** it upstream

`git push origin master`

You can also specify `-v`, which shows you the *URL* that Git has stored for the short name to be expanded to.

If you want to *rename* a reference, in newer versions of Git you can run `git remote rename` to change a remote’s shortname. 

`git remote rename pb dave`

`git remote origin dave`

It’s worth mentioning that this changes your remote branch names, too. What used to be referenced at pb/master is now at paul/master.

If you want to remove a reference for some reason — you’ve moved the server or are no longer using a particular mirror, or perhaps a contributor isn’t contributing anymore — you can use git remote rm:

`git remote rm dave`

`git remote origin`

### Pulling a repo

To pull all the changes made to the repository then we can use the `pull` command

`git pull`

It is good practice to pull the repository before pushing changes
- You get an up to date copy of the repo to push to
- You can see any conflicts before they are pushed
- You can `stash` your changes before pulling the remote branch 

---

# Git Branching

![Git Branching](../../images/git5.png)

Branching allows you to diverge from the main line of development
- Without doing accidental damage to the main line

Git branches are very lightweight compared to other VCS
- Git encourages a workflow that allows you to branch and merge

Branches build on core Git features 
- When you commit you have a snapshot of current content
- Plus zero or more pointers to the current commits
- Based on this repository or parents

Create new branch

`git checkout -b newBranchName`

Commit any changes to your code

`git commit -am "updated some file(s)"`

Merge branch back into main line

`git checkout master`

`git merge newBranchName`

Branching allows you to diverge from the main line of development without doing accidental damage to the main line.

Branches build on core Git features:
- When you commit you have a snapshot of current content.
- Plus zero or more pointers to the current commits.

Rather than use the `checkout –b` command shown above, you could use the following two commands:

`git branch newBranchName`

`git checkout newBranchName`

---

# Choosing a DVCS

### GitHub

![GitHub Splash Screen](../../images/github_splash.png)

### BitBucket

![BitBucket Splash Screen](../../images/bitbucket_splash.png)

### GitLabs

![GitLabs Splash Screen](../../images/gitlab_splash.png)

When using git server or a hosting site, you might follow these steps:

1. Create a repository on a hosting site, or own server
2. **Check out** the repository to your own machine using `git remote add`
3. **Add** some code
4. **Commit** your changes to the local repository
5. **Push** changes to the remote repository using `git push`

---

# The SSH Protocol

The SSH File Transfer Protocol is a network protocol 
- Provides file access, file transfer, and file management functionalities
- It is a UNIX-based command interface and protocol 
- Used for securely getting access to a remote computer.

It is widely used to control Web and other kinds of servers remotely
- SSH is actually a suite of three utilities - slogin, ssh, and scp 
- SSH commands are encrypted and secure
  - Authenticated using a digital certificate
  - Passwords are protected by being encrypted
  - SSH uses RSA public key cryptography 
    - For both connection and authentication

---

# Alternatives

**Git** is not the only source control method out there
- Popular due to the open source nature
- Simple to use
- Context switching between branches easier
- Local staging area for commits 
- GUI tools available such as Sourcetree
- Built in tools in eclipse

## Subversion (SVN)

- Similar idea to Git
- Add new files, commit to the repository
- Pull files from the repository
- Tortoise / Rabbit SVN give built in windows context menu options

## CVS, Mercurial, Bazaar, Fossil, Veracity… many others!

---

