<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_pink.png" width="100"/></a>

# Build Types

## Which build scope might you use and on which frequency?

|           |          |       Scope |      |     |
| --------- | -------- | ----------: | ---- | --- |
|           |          | Incremental | Full |     |
| Frequency | Commit   |             |      |     |
|           | Hourly   |             |      |     |
|           | Daily    |             |      |     |
|           | Weekly   |             |      |     |
|           | Manually |             |      |     |

*Whole Activity Time Box:* **3 minutes**
