<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Pair Programming

## General

This topic is drawn from the Build Process section of the ICAgile learning matrix.

---

## Connections - Build Tools

**Activity:** Build Types - Which build scope might you use and on which frequency. (Paper)

**Question:** Why do we use need build tools? (PollEv)

**Question:** What build tools have you heard of? (PollEv)

---

## Concepts - Build Tools

Acknowledge all valid points and point out any that they might have missed. Would expect to discuss merge conflicts, overwriting work, branching etc.<perhaps note some example>

Any required information can be found in <a href="./02_4_BuildToolsAndVersionControl_ComplimentaryMaterials_Concepts.md">here</a>

**Demo:** Build tools (use language you are most familiar with)
Demo their exercise. Convert calculator. Paste in dependency

**NOTE:** *Given that plugins update periodically, encourage learners to identify most recent version of whatever plugin you ask them to use. E.g. JUNIT, SureFire. Good practice, always go with latest (unless it breaks something then dial it back). Mention OWASP if you like.*

*Demo:* Google “junit maven dependency"

Also create new project to show structure AND best practice of separate folder for test classes. [10 minutes]

---

## Concrete Practice - Build Tools

**Exercise:** Build Tools - <a href="./02_5_BuildToolsAndVersionControlConcretePractice.md">02_5_BuildToolsAndVersionControlConcretePractice.md</a>

---

## Connections - Version Control

**Question:** How do developers handle working on the same code?
**Activity:** List the characteristics of the Version Controls systems

---

## Concepts - Version Control

Acknowledge all valid points and point out any that they might have missed. Would expect to discuss merge conflicts, overwriting work, branching etc.<perhaps note some example>

Demo some of the Git commands

---

## Concrete Practice - Version Control

**Exercise:** Version Control - <a href="./02_7_VersionControlConcretePractice.md">02_7_VersionControlConcretePractice.md</a>

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.
