<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Delivery/NewQAIcons_Delivery_pink.png" width="100" /></a>

# Build Tools

## Learning objectives

* Apply tools for build automation and version control.
* Use different tools to produce an automated build.
* Apply version control using both approaches (client/server or distributed) and contrast the two approaches.

---

# The Build Process

* Objectives
  - Examine Tools to produce automated builds
  - Understand the values of version control
  - Describe and use Continuous Integration tools
    + Talk about the process of continuous integration and continuous delivery
    + Understand and be able to use the basics of git based source control
    + Know about the principles of build managers

* Contents
  - Dependency Managers (Maven, npm, PIP, NuGET)
  - Version control – using GIT
  - Continuous Integration – using Jenkins

* Exercise (Installing Jenkins)

---

# Automated Build Tools

There are various build tools that help to produce an automated build and version control.  Some of the popular ones are:

* Apache Ant

* Apache Maven (Successor to Ant)

* Gradle

Build automation allows automating the creation of a software build and the associated processes which could include:
* Compiling, packing and running automated tests

---

## Why use Automated Build Tools

Developers often manually do the build process. On large projects this is not practical, it is hard to keep track of dependencies, what needs to be built, the sequence of the build. Using an automation tool allows this build process to be easier and more consistent.

Dependency management
* Where to look for dependencies

Access control
* Access what projects depend on you and control visibility and access

Custom control
* Allows custom build mechanisms allowing custom archives to be built

Packaging
* Package the compiled source code

Running tests

---

# What are build managers?

Build managers automate building your project
* Repeatable
* Automatically running the tests
* Provides reports on builds passing or failing
* Capable of building code bases on different architectures

Tie in with build automation tools
* Maven / Gradle / SBT etc.
* Resolve all dependencies for a project

---

# Different build managers

## Jenkins 
* Java based
* Create your own server
* Created from the Hudson project
* Very popular, well supported, a plugin for anything

## Hudson
* Original Jenkins project, owned by Oracle

## CircleCI & TravisCI
* Integration with github straight away
* Online service

## Cruise Control
* One of the first build managers
* Integration with ant, make maven etc.

## Ant / Make etc.
* Original build managers for C/C++ projects and multi language projects

---

# Requirements for Build Managers

Needs to be capable of: 
* Notice changes in source code
* Get a copy of the source code
* Build it (resolving dependencies)
* Test it
* Start deployment of code (often using docker, chef, puppet etc.)
* Provide some big visible charts showing the status of all the projects

---

<!-- # Using Maven in Eclipse

Tutor Demonstration of Maven project

Explanation of POM file

Dependency management -->

# Using a Build Manager

Dependent on your trainer's area of expertise they will demonstrate:

- The use of a build manager
- How dependencies for a project are recorded and stored
- How dependencies can be managed

The following links may be of use for different environments:

NuGet - C# and Visual Studio - https://docs.microsoft.com/en-us/nuget/quickstart/install-and-use-a-package-in-visual-studio

Maven - Java - http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html

npm - JavaScript - https://docs.npmjs.com/using-npm-packages-in-your-projects

pip - Python - https://packaging.python.org/tutorials/installing-packages/

---

# What is CI/CD?

## Continuous Integration
- Developers work on a new feature
- It is added to the main code base automatically
- Test driven development methodologies used to verify feature works and doesn’t break anything
- Automating the build process

## Continuous Deployment / Delivery
- Successful, repeatable deployment of code
- After the build has run and been verified then deploy to server
- Automate the server setup / connection
- Code releases can be minute by minute, rather than months at a time

---

# Best Practices – Continuous Integration

## Maintain a Single Source Repository
- No more emailing code around!
- Should contain everything required for the build including test and compilation scripts

## Everyone Commits To the Mainline Every Day
- Commit each change when it works

Adapted From: http://martinfowler.com/articles/continuousIntegration.html

---

# Best Practices – Building and Testing

- Automate the build
  - Don’t allow human error to miss steps or get the wrong libraries!

- Make the build self-testing
  - Follow TDD principles, everything that can be tested should be

- Test in a Clone of the Production Environment
  - Test environment should mimic the production

- Every Commit Should Build the Mainline on an Integration Machine
  - This is where CI tools come in handy
  - Each commit should trigger a build – fail fast again

- Keep the Build Fast
  - Nothing is more dull than watching the build process
  - If the build is slow then people won’t commit code until they have to

---

# Best Practices - Culture

- Fix Broken Builds Immediately
  - Everyone should be involved with fixing problems

- Make it Easy for Anyone to Get the Latest Executable
  - Make it possible to allow people to demo the code
  - Everyone is using the same current copy

- Everyone can see what's happening
  - Communication!
  - Big Visible Charts!
  - This is the CI/CD version of agile best practices. Everyone should be informed at all times

- Automate Deployment
  - Set off deployment scripts
  - Repeatable, testable ideas

---

# Tools for the job

We will need tools for both the integration and development side

## Source control
Deals with the integration of code from one or more person
We will be using git based source control
Alternatives exist:
- Subversion (svn)
- Mercurial 
- Team Foundation Server

## Build Tools

- Realises a change has occurred in the code base
- Clones the code
- Compiles, runs test and reports if there was a success or failure

---

# Introducing Jenkins

![Jenkins](../../images/jenkins.png)

First released in 2005 as the Hudson project
- Created by Sun Microsystems
- Oracle bought the company and so had control of the Hudson name
- Name changed to Jenkins for the open source project
- Both the Hudson and Jenkins project are still active but development has diverged

Created as a CI / CD tool
- Aim was to be easy to install and use
- Plugins to connect to source control
- Builds projects using the project setup – so can work with any language
- Public / company facing dashboard lets everyone know the status of the current build

---

# What does Jenkins do?

Jenkins is a one stop shop for testing and building code
- It can even deploy code onto servers if they already exist

Usual workflow:
- Developer checks some new code into git
- Git informs all watchers that it has updated
- The Jenkins git plugin starts the build process
- Clones the project
- Compiles, runs tests
- Updates the dashboard with any feedback
- Start the post-build steps or actions to deploy

---

# Why is Jenkins popular?

Allows companies to follow some of the best practices for CI/CD
- Automate the build
- Make the build self testing
- Keep the build fast
- Make sure everyone can see the results of the build
- Automate deployment

Easy installation and config
- Can be done all through the GUI – no need for more XML files

Many plugins available for any combination of source control, compiling, testing and deployment
- Still a very active project!
- IDE Integration, orchestration tools, style checkers

---

# Distributed Jenkins Service

Jenkins uses agents to scale systems
- By default there is one agent which will build using one core 
- May need to build for different architectures
  - Windows / Mac / Linux
  - Android / iOS / Windows Phone
- Master / Node servers
- Can exist anywhere

Single report can be generated over all the builds

---

# Version Control

## Why use version control?

![Argh!](../../images/argh.png)

Keep track of code and changes
- One copy of the code everyone has access to
- No more mailing around code and confusion trying to integrate it
- Automated version management 

Allows for multiple people to edit a single project at the same time
- Push changes to the central repository
- Everyone can pull changes from others
- Merge together changes in files where there are conflicts

Branch code to work on specific parts
- Version 2.3 doesn’t need to die because someone else wants to look at version 3

---

# Source Control

Don’t mail around versions of your code!
- Automated version management 

Allows for multiple people to edit a single project at the same time

Push your changes to a central repository
- ONLY push changes when they work

Pull changes from other people from the central repository

Merge changes together where you’re both using the same files
- Source control works best with plain text

Branch code to work on specific parts
- Version 2.3 doesn’t need to die because someone else wants to look at version 3

---

# GIT as a DVCS

GIT’s origins are in Linux Development and is open source

Its goals were to create a DVCS system that was:
- Fast
- Simple
- Strong support for non-linear development
- Fully distributed
- Able to handle large projects like the Linux kernel efficiently

---

# Version Control

Version control is a system that records changes to files

A VCS allows you to manage file history allowing you to:
- Rolling back to previous states of a file if something goes wrong
- Maintain change logs allowing you compare versioning issues

The simplest form of version control systems are local e.g. rcs in Mac OS x

![Tradional Version Control](../../images/git1.png)

---

# Centralised Version Control Systems

Central VCS systems allow multiple developers to collaborate on other systems.
- e.g. CVS, Subersion and Perforce
Single server that contains all versioned files
- Clients that check out files from this source

![Centralised Version Control Systems](../../images/git2.png)

---

# Distributed Version Control Systems 

DVCS do not just check out the most local snapshot of a file
- They mirror the repository 
- so each checkout is like a backup of the repository

![Distributed Version Control Systems](../../images/git3.png)

---

# Git Basics

Initialising repositories
Creates a new subdirectory and creates a git repository skeleton

`git init`

Initialising a repository with existing files

`git add *.pp`

`git add README.md` 

`git commit -m "Initial commit"` 

Cloning an existing repository
- Git can use a number of protocols including http and SSH

`git clone git://github.com/resource`

---

# Recording Changes to a Repository

Each file in a  git directory can be tracked or untracked
- Tracked files are files that were in the last snapshot
  - They can be unmodified, modified or staged
- Untracked files are everything else
  - Not in your last snapshot or staging area

![Git file lifecycle](../../images/git4.png)

The main tool you use to determine which files are in which state is the git status command. If you run this command directly after a clone, you should see something like this:

`git status` 

On branch master nothing to commit, working directory clean

Note: The graphic shown above is taken from the following article:

https://git-scm.com/book/en/v2/Git-Basics-Recording-Changes-to-the-Repository


## Staging new or "modified" files

`git add <filename>`

You can have GIT ignore files or folders through the ```.gitignore``` file
- Add them to the file
- This can be automated

---

# Working with Remote Repositories

Git projects are often held on remote repositories 
- These hold versions of a project or dependencies on the web/network

To see configured remote repositories run the `git remote` command
- If you have cloned a repository you should see the origin
- To add a repository:

`git remote add [shortname][url]`

*shortname* becomes an alias for access to the repository

### Pushing to a repo

When you have your project at a point you want to share you have to **push** it upstream

`git push origin master`

You can also specify `-v`, which shows you the *URL* that Git has stored for the short name to be expanded to.

If you want to *rename* a reference, in newer versions of Git you can run `git remote rename` to change a remote’s shortname. 

`git remote rename pb dave`

`git remote origin dave`

It’s worth mentioning that this changes your remote branch names, too. What used to be referenced at pb/master is now at paul/master.

If you want to remove a reference for some reason — you’ve moved the server or are no longer using a particular mirror, or perhaps a contributor isn’t contributing anymore — you can use git remote rm:

`git remote rm dave`

`git remote origin`

### Pulling a repo

To pull all the changes made to the repository then we can use the `pull` command

`git pull`

It is good practice to pull the repository before pushing changes
- You get an up to date copy of the repo to push to
- You can see any conflicts before they are pushed
- You can `stash` your changes before pulling the remote branch 

---

# Git Branching

![Git Branching](../../images/git5.png)

Branching allows you to diverge from the main line of development
- Without doing accidental damage to the main line

Git branches are very lightweight compared to other VCS
- Git encourages a workflow that allows you to branch and merge

Branches build on core Git features 
- When you commit you have a snapshot of current content
- Plus zero or more pointers to the current commits
- Based on this repository or parents

Create new branch

`git checkout -b newBranchName`

Commit any changes to your code

`git commit -am "updated some file(s)"`

Merge branch back into main line

`git checkout master`

`git merge newBranchName`

Branching allows you to diverge from the main line of development without doing accidental damage to the main line.

Branches build on core Git features:
- When you commit you have a snapshot of current content.
- Plus zero or more pointers to the current commits.

Rather than use the `checkout –b` command shown above, you could use the following two commands:

`git branch newBranchName`

`git checkout newBranchName`

---

# Choosing a DVCS

### GitHub

![GitHub Splash Screen](../../images/github_splash.png)

### BitBucket

![BitBucket Splash Screen](../../images/bitbucket_splash.png)

### GitLabs

![GitLabs Splash Screen](../../images/gitlab_splash.png)

When using git server or a hosting site, you might follow these steps:

1. Create a repository on a hosting site, or own server
2. **Check out** the repository to your own machine using `git remote add`
3. **Add** some code
4. **Commit** your changes to the local repository
5. **Push** changes to the remote repository using `git push`

---

# The SSH Protocol

The SSH File Transfer Protocol is a network protocol 
- Provides file access, file transfer, and file management functionalities
- It is a UNIX-based command interface and protocol 
- Used for securely getting access to a remote computer.

It is widely used to control Web and other kinds of servers remotely
- SSH is actually a suite of three utilities - slogin, ssh, and scp 
- SSH commands are encrypted and secure
  - Authenticated using a digital certificate
  - Passwords are protected by being encrypted
  - SSH uses RSA public key cryptography 
    - For both connection and authentication

---

# Alternatives

**Git** is not the only source control method out there
- Popular due to the open source nature
- Simple to use
- Context switching between branches easier
- Local staging area for commits 
- GUI tools available such as Sourcetree
- Built in tools in eclipse

## Subversion (SVN)

- Similar idea to Git
- Add new files, commit to the repository
- Pull files from the repository
- Tortoise / Rabbit SVN give built in windows context menu options

## CVS, Mercurial, Bazaar, Fossil, Veracity… many others!

---

