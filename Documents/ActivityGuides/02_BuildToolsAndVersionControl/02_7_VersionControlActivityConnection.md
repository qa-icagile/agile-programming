<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_green.png" width="100"/></a>

# Version Control

## List the characteristics of the Version Controls systems

To add to the list, please go to:

**https://pollev.com/\<instructor-link>** and type your response there for your pair,

OR:

Send a text with the pre-fix **\<instructor-link>** followed by a space and your response to **07840 781235**.