<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Test Coverage

## General

---

## Connections Test Coverage

**Question:** What is the purpose of assessing test coverage? (PollEv)

To find untested code!

---

## Concepts Test Coverage

Acknowledge all valid points and point out any that they might have missed. Would expect to hear about counting the lines covered by test. Low number would suggest that not all paths are covered. 

Mention BICEP<perhaps note some example>


Code Coverage tools:
- Clover
- Eclemma for Eclipse 


A demo to show how to integrate Clover and Eclemma in Eclipse 
No instructions for Demo. Figure out a demo for the chosen IDE. That could should be Clover and eclemma. Enable the tool. Run the tests. No need to write any tests. These are for Java. Find equiv. for other languages and IDEs

---

## Concrete Practice Test Coverage

Start with Calculator project that they currently have in their repo.

The should what their current code coverage is for the project.

They can monitor that value during future exercises.

They should rename those poorly named tests.

Instructions are contained in the file <lang>05_TestCoverage.md in the \Guides folder extracted from their student files.

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.