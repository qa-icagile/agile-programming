<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Question/NewQAIcons_Questions_green.png" width="100"/></a>

# What is the purpose of test coverage?

To answer this question, please go to:

**https://pollev.com/\<instructor-link>** and type your response there for your pair,

OR:

Send a text with the pre-fix **\<instructor-link>** followed by a space and your response to **07840 781235**.

*Whole Activity Time Box:* **3 minutes**
