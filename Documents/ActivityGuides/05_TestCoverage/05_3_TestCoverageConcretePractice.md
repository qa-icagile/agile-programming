<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_green.png" width="100" /></a> 

# Test Coverage Exercise

Work in pairs, continue working on the Calculator project in your chosen language.

Locate the **Guides** folder for your language and then the *Test Coverage* file.

Follow the instructions to write find out how to assess code coverage.

*Whole Activity Time Box:* **20 minutes**