<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Readable Tests

## General

What are the key phases of readable tests - verifying tests is done later

---

## Connections Readable Tests

**Question:** What are the four phases of a readable test? (PollEv)

---

## Concepts Readable Tests

Acknowledge all valid points and point out any that they might have missed. Would expect to hear about readability, maintainability, self documenting code, can understand what failed quickly based on the test name. Might use names to help group tests


*Demo:* tests with Setup etc.

*Demo:* custom assertion – validating the given postcode .
- Demo that uses the custom assertion for postcode sits away from the actual test(s) making the test(s) more readable. Shorter too.

---

## Concrete Practice Readable Tests

Students will continue working on Calculator Project

Students should demonstrate built-in and custom assertions

Instructions are contained in the file <lang>4_ReadableTests.md in the \Guides folder in the repo.

Students will rename some tests. Write a custom assertion. Perhaps use `setup()` for their fixtures.

**Exercise:** They could/should create new repo for this exercise. i.e. they will need to copy the code from the downloaded bundle into their new repo.


---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.