<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_blue.png" width="100" /></a> 

# Readable Tests Exercise

Work in pairs, choosing a language to complete this task in.

Locate the **Guides** folder for your language and then the *Readable Tests* file.

Follow the instructions to write more readable tests.

*Whole Activity Time Box:* **20 minutes**