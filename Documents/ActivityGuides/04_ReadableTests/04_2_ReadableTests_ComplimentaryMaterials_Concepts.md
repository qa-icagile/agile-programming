<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Delivery/NewQAIcons_Delivery_blue.png" width="100" /></a>

# Readable Tests

## Learning objectives

* Write readable test code to exercise built-in and custom assertions. 
* Prepare tests expressing the four phases of each test (Setup, exercise, verify and cleanup).
* Apply built-in and custom assertions in code.

---

# Coding Tests by intention

Tests should be coded consistently with a well defined clear intent

Four phases are standard:

- Setup 		  – Setup the initial state for the test
- Exercise		- Perform the action under test
- Verify 		  - Determine and verify the outcome
- Cleanup  	  - Clean-up the state created

Each phase should be:

* Clearly Expressed
* Well documented

Expected Outcomes should be clearly expressed

---

# Right B.I.C.E.P.

![Right B.I.C.E.P](../../images/readable-tests-right-bicep.png)

Guidelines of some areas that are important to test:

* **Right** - *Are the results right?*
* **B** - Are all the **B**oundary conditions CORRECT?
* **I** - Can you check the **I**nverse relationships?
* **C** - Can you **C**ross-check results using other means?
* **E** - Can you force **E**rror conditions to happen?
* **P** - Are **P**erformance characteristics within bounds?

---

