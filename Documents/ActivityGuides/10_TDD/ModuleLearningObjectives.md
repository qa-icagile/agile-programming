# Test Driven Development

## Objectives

- Apply TDD using the red-green-refactor steps and relate the impact of shortening cycle times
- Explain how TDD can be implemented and why it adds value to a codebase
- Apply TDD and experience the nature of the red-green-refactor cycle and the feeling of the shift to shorter and shorter cycle times
