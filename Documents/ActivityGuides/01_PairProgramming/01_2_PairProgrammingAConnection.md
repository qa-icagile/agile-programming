<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/DevelopSkills/NewQAIcons_DevelopSkills_orange.png" width="100" /></a> 

# Set Up the Working Environment
Given the following Pair roles allocate each role to a zone based on expertise level of the Driver and the Passenger in the pairing (Expert or Novice):

- Driver-Navigator
- Tour Guide
- Backseat Navigator
- Ping Pong

## Zones

Driver - Passenger
* Expert - Expert
* Expert - Novice
* Novice - Expert
* Novice - Novice

|               |        | **Driver** |        |
| ------------- | ------ | ---------- | ------ |
|               |        | Expert     | Novice |
| **Passenger** | Expert |            |        |
|               | Novice |            |        |

You will be asked to put your pair's answers into PollEv.

### Can you explain why you have chosen your allocations?

*Whole Activity Time Box:* **3 minutes**