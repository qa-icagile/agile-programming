<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Apply/NewQAIcons_Apply_orange.png" width="100" /></a> 

# Pair Programming - Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

*Pair Activity Time Box:* **5 minutes**

Find (and open) your **Lessons Learned** document and make a couple of notes as to what you are taking away from the session on Pair Programming.  This can include:

- Things you want to apply in your working practices
- Things you want to find out more about before assessing whether to incorporate into your working practices
- Anything that you found interesting or important when thinking about Pair Programming.

*Personal Activity Time Box:* **3 minutes**

*Whole Activity Time Box:* **10 minutes**