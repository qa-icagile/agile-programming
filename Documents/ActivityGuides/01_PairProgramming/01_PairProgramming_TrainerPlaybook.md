<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Pair Programming

## General

This topic is drawn from the Collaborative Development section of the ICAgile learning matrix.

---

## Connections

**Question:** What are the benefits and disadvantages of pair programming? (PollEv)

**Activity:** Matching pair roles

Ask them to spend 3 minutes on an activity matching the skill level of pairs and describing expert-expert, novice-novice, expert-novice combinations.

---

## Concepts

Any required information can be found in <a href="./01_3_PairProgramming_ComplimentaryMaterials_Concepts.md">here</a>

Acknowledge all valid points and point out any that they might have missed. 
Examples: 
* Better code practice 
* Mentoring 
* Team cohesion 
* Collective code ownership

Acknowledge all matchings and point out any that they might have missed. 
Examples: 
* Driver/Navigator
* Backseat Navigator
* Tour Guide
* Ping Pong

---

## Concrete Practice

**Exercise:** Pair programming

Set each pair a short programming exercise. One should code and the other narrates. Then swap. For virtual/AfA, swap sharing of the desktop

Instructions are contained in the file *<a href="./01_4_PairProgrammingConcretePractice.md">01_4_PairProgrammingConcretePractice.md</a>* in their student files folder.

The third task is only required if there is an odd number. Effectively refactoring for single responsibility.

**Outcomes:** 

That they manage to find instruction file, split into team areas on webex, tech works, that they emerge with some ideas about what pair programming involves. Benefits, pitfalls.


Discuss during Conclusions.

Have a fixed timebox with timer. We have allocated 20 minutes to this exercise.
If lots of them don’t finish, we’ll take that on board and possibly change it for other courses.

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.
