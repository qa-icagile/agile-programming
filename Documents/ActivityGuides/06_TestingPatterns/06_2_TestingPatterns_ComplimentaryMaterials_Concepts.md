<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Delivery/NewQAIcons_Delivery_orange.png" width="100" /></a>

# Testing Patterns

## Learning objectives

* Identify testing patterns and design readable, efficient, robust tests that reveal the quality of the underlying code.
* Identify both the common test smells and how to address them through design changes.
* Practice and apply different ways of verifying test code.
* Practice and apply some of the indicators and refactoring to intention-revealing tests.

---

# Testing Patterns

## Resulting State Assertion
- **Standard test:** this is the state we expect

## Guard Assertion
- Assert precondition for the test to be correct
- Then Resulting State Assertion
- E.g. a `remove()` method really removes something that was there

```java
assertThat(basket.list(), hasItem(new Product("4 Candles", 9))); 
basket.remove(1);
assertThat(basket.list(), not(hasItem(new Product("4 Candles", 9))));
```

- Matchers generally make custom message superfluous, but here we can clarify the role of the guard assertion

```java
assertThat(basket.list(), describedAs("A pre-requisite for the test", hasItem(new Product("4 Candles", 9)))); 
```

Kent Beck recommends writing the assert as the first step in writing a test method – i.e. start by identifying, 

>"What is the right answer?" 

Recall that the ideal is to have exactly one assertion per test, but this is not an absolute, and the guard assertion pattern is a good illustration of the exception that proves the rule.  

---

# Testing Patterns

## Delta assertion
- If absolute Resulting State cannot be guaranteed
- Test the delta between initial and resulting states

## Custom assertion
- If code verifying assumptions is long, extract to method
- The method should contain an `assert()`
- If it contains logic, it too should be tested
- Consider writing as a custom matcher
  - Will combine with other matchers

```java
assertThat("12345", not(isAPostCode()));
assertThat(codes, hasItem(isAPostCode()));
```

The delta assertion pattern would be used where there is something slightly outside the control of the test, where its initial state is not determinate, but where the operation we are testing will have a determinate effect upon it.  Performance tests are often implicitly delta assertions, in that they take a timing before an operation, and the timing again after it, and assert that the difference is below a certain threshold.  

The custom assertion pattern helps our test code respect DRY, in that there may be some common functionality which otherwise would be duplicated.  In the envisaged example, perhaps we need to assert that all the Cards in an initially generated Deck are different, and again, when the shuffle operation has been performed.  Obviously one has to be careful about introducing complexity into the tests, without having a test to verify that this functionality itself is correct.  


---

# Testing Patterns

## Parameterised test

- Data-driven testing: one test method, multiple data sets
- Data hard-coded in 2D array, or from external source (file, db)

```java
@Parameters
public static Collection makeData() {
 return Arrays.asList( new Object[][] {{ 1,  "Jan" },{ 3,  "Mar" }, 
                                       { 12, "Dec" } });
    } // { input to fn, expected output }
public UtilsTest(int input, String output) {
   this.input = input;
   this.expected = output;
}
```

JUnit 4 provides for parameterised tests to be set up with annotations.  The test class itself must be annotated as follows:

```java
@RunWith(value=Parameterized.class)
public class UtilsTest {
   private int input;
   private String expected;
```

The test method looks like an ordinary test method, except that it uses instance variables of the class. 

```java
@Test
public void testGetMonthString() {
   assertEquals("Incorrect month String", expected, Utils.getMonthString(input));
}
```

For each array of values from the `@Parameters` annotated method, a test is run where those values are passed to the constructor, and any `@Before`, then `@Test`, then `@After` method in the class invoked.  Equally importantly, we could also define a parameterised exception test, in which the `@Parameters` annotated method defines negative data for the function – e.g. a negative number; the corner case 0 (in many systems January is indexed by 0); and a very large number.  In these cases there is no expected value to match with the input; we expect the function to throw an Exception.  

---

# Testing Patterns

## Parameterised creation method

- Factor out fixture object creation from setUp to PCM
- Hides attributes essential to fixtures, but irrelevant to test
- Useful when creating complex mock esp. if it will be used in multiple tests 
- Consider also Builder Pattern

## Object mother

- Factors out creation of business objects to factory class, or just a class containing fixtures to avoid duplication
- E.g. a set of standard "personas":

```java
private static List<Skill> bobskills = Arrays.asList(CARPENTER, PLUMBER);
public static Builder BOB = new Builder("Bob", bobskills);
```

With the *Parameterised Creation Method*, we can encapsulate complex fixture creation into a self-explanatory method.  Here’s an example using the **JMock** framework to create something which fakes the ResultSet you might get by running a SQL query like `“SELECT id, surname FROM users”` against a database: 

```java
private ResultSet generateMock2By2ResultSet() throws SQLException {
	final ResultSet mockResultSet = context.mock(ResultSet.class);	

	context.checking(new Expectations() {{
		oneOf (mockResultSet).next(); will(returnValue(true));
		oneOf (mockResultSet).getString(1); will(returnValue("fred001"));
		oneOf (mockResultSet).getString(2); will(returnValue("Foggs"));
		oneOf (mockResultSet).next(); will(returnValue(true));
		oneOf (mockResultSet).getString(1); will(returnValue("bill100"));
		oneOf (mockResultSet).getString(2); will(returnValue("Boggs"));
		oneOf (mockResultSet).next(); will(returnValue(false));
	}});
	return mockResultSet;
}
```

---

# Testing Patterns

## Extra constructor

- If existing constructor hard-codes some dependencies
- Allows dependency injection by test (mock or stub)
- Introducing a ‘trapdoor’ to make code easier to test:
  
>"My car has a diagnostic port and an oil dipstick. There is an inspection port on the side of my furnace and on the front of my oven. My pen cartridges are transparent so I can see if there is ink left." – Ron Jeffries

## Setter Injection

- **Constructor injection:** API signals that the parameter(s) is not optional: to be supplied when creating the object
- **Setter injection:** API signals the dependency is optional, changeable

Ron Jeffries is quoted by Massol, *JUnit in Action p.148*, originally from
*http://tech.groups.yahoo.com/group/testdrivendevelopment/message/3914*

>He continues "And if I find it useful to add a method to a class to enable me to test it, I do so. It happens once in a while, for example in classes with easy interfaces and complex inner function (probably starting to want an Extract Class).“

To return to the number generator example explored with mocks, with either constructor or setter injection we can have a default constructor which hard-codes the default dependency, and for instance a setter which allows that to be overridden (with a mock, in the test):

```java
public class Lottery {
	private NumberGenerator generator; 
	public Lottery() {
		this.generator = new RandomNumberGenerator(); 
	}
	public void setGenerator(NumberGenerator generator) {
		this.generator = generator; 
	}
      // etc. 
```

---

# Testing Patterns

## Test-specific subclass

- Not permitted to modify actual class
- Create behaviour-modifying or state-exposing subclass
- More likely when testing didn't drive development
- E.g. lottery class 
  - Default constructor creates real `RandomNumberGenerator`
  - `NumberGenerator` field has `protected` visibility
  - `TestableLottery` extends `Lottery`

```java
public TestableLottery(NumberGenerator generator) {
	this.generator = generator; 
}
```

  - Inherits method to test as-is
  - Test creates instance of testable subclass, injecting mock

The test-specific subclass approach is also illustrated on the next page, where the dependency is instantiated in a factory method.  In both cases, the sub-classing is about where the dependency comes from; the key point is that the code we wish to test is inherited untouched in the subclass.

---

# Testing Patterns

**Use factory pattern for dependencies**
- E.g. CUT uses factory method
  - *“Extract and override”*: extract dependency creation to factory
  - Override method in test-specific subclass
  - **Or:** Client asks factory class for dependency

```java
public Lottery() {
	generator = NumberGeneratorFactory.create(); 
} 
```

**Factory** provides means for test to change type returned
- Test sets factory to return mock or stub
- Application code semantics completely unchanged 

## The factory method approach in more detail.  

The CUT has:

```java
public class Lottery {
	NumberGenerator generator; 
	protected NumberGenerator getGenerator() {
		return new RandomNumberGenerator(); 
  }
  // etc.
```

  The test-specific subclass can simply @Override that factory method.  Using Java inner classes, we don’t even need a separate class for the test-specific subclass; it can be created e.g. as an anonymous inner class: 

```java
@Test public void generateArrayAsString() {
	final NumberGenerator mockGenerator = 
						createMock(NumberGenerator.class);
	Lottery lotto = new Lottery() {
		@Override protected NumberGenerator getGenerator() {
			return mockGenerator;
		}
	};
	expect(mockGenerator.generate(anyInt())).andReturn(2).times(6);
	// etc.
```

---

