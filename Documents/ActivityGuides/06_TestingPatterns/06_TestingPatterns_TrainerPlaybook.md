<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Testing Patterns

## General

---

## Connections Testing Patterns

**Question:** List some common test patterns (PollEv)

- Can they think of any testing patterns?
- What are they?
- Why do we like/need patterns?
- How do they differ?


---

## Concepts Testing Patterns

Acknowledge all valid points and point out any that they might have missed. Would expect to hear guard assertions, test roulette, custom assertions

Examples 
- Testing-file system
- Testing-database
- Unit extensions (JUnitPerf, DBUnit and XMLUnit)
- Parameterised Tests
- Factory Patterns

---

## Concrete Practice Testing Patterns

No exercise for this module at the moment.

We will stick with the discussion.

---

## Conclusions

In pairs reflect on how these concepts relate to their role and what impact, if any, they may have.

Also reflect on how this topic, if at all, relates to the previous topics.

Finally, take a 3 minute time box to log individual take away from this session in Learning log.