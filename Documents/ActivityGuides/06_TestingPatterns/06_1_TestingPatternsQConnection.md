<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Question/NewQAIcons_Questions_orange.png" width="100"/></a>

# List some common test patterns

To add to the list, please go to:

**https://pollev.com/\<instructor-link>** and type your response there for your pair,

OR:

Send a text with the pre-fix **\<instructor-link>** followed by a space and your response to **07840 781235**.

*Whole Activity Time Box:* **3 minutes**
