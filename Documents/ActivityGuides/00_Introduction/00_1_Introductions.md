<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>
<a href="#"><img src="../../images/NewQAIcons/Caring/NewQAIcons_Caring_blue.png" width="100" /></a> 

# Introductions

1. Split into pairs - if working virtually, help will be given to assign partners.
2. Interview each other to find out:
  * Who the other person is;
  * Why they are here;
  * What they are wanting to learn from the course;
  * An interesting fact about them.
3. You have a *timebox* of 3 minutes.
4. You will then introduce the other person to the whole group.

*Whole Activity Time Box:* **30 minutes**
