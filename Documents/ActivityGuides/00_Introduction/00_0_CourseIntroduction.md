<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Agile Engineering
## icAgile - Agile Programming

![This course is accredited by icAgile](../../images/icAgileAccCourse.png)

---

## Course Delivery

![The 4 Cs](../../images/CCCC.png)

![Training from the back of the room](../../images/TFTBOTR.png)

---

## Health, safety and environment

![Health, safety and environment](../../images/HSE.png)

Please pay attention to the standard health and safety briefing, as it is for your own benefit and that of your colleagues on the course. Also, please enjoy the lunches!

---

# icAgile Roadmap

ICAgile offers both knowledge-based and competency-based certifications:
- Certifications that start with “ICAgile Professional Certifications (ICP)” - Knowledge-based certifications focusing on fulfillment of ICAgile learning objectives and an in-class demonstration of acquired knowledge. (Pictured as silver ribbons)
- Certifications that start with “ICAgile Expert Certifications (ICE)” - Competency-based certification requiring considerable field experience and a demonstration of competency in front of an expert panel. (Pictured as gold ribbons)

![icAgile Learning Roadmap](../../images/icAgileRoadmap.png)

---

# Course Backlog:

- Unit Testing
- Readable Tests
- Test Coverage
- Testing Patterns
- Speed of Builds
- Test Doubles
- Code Smells
- Refactoring with Existing Tests
- Dealing with Legacy Code
- TDD
- BDD
- Living Documentation
- ATDD
- Collective Responsibility
- Pair Programming
- Build Tools and Version Control
- Continuous Integration

---