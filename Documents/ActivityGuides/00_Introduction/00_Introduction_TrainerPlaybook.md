<a href="#"><img src="../../images/NewQAIcons/qalogo.jpg" width="100" /></a>
<a href="#"><img src="../../images/icAgileLogo.jpg" height="100" /></a>

# Trainer Playbook - icAgile Programming - Introduction

## Introduction

This course is a combination of two ICAgile courses: Agile Programming and Agile Design. The modules have been drawn from both and have been arranged to provide a more holistic approach to the combined topics.

This playbook lists each of the sessions to be covered in the course. Although they have been listed in a particular order, feel free to rearrange the order if you feel it is appropriate. Furthermore, feel free to include additional sessions or omit as you see fit.

Each session has been described in terms taken from the book:  Training from the back of the room!. I.e., it is presumed that you will structure each session around the 4Cs: Connections, Concepts, Concrete Practice, and Conclusions.

<br />

![Training from the Back of the Room! - Sharon L. Bowman](../../images/TFTBOTR.png)

<br />

This approach is becoming increasingly popular and is the preferred approach when designing Agile training courses and workshops.

The suggestions contained in this playbook are precisely that. Suggestions.

If you have alternative exercises or running order, then please go ahead and use them. The most important thing is that the objectives of the course are achieved and that the students leave the class feeling that they have learned something and that they have enjoyed the experience.

---

## General Ideas

Respect the timebox. When taking breaks and pairing off for exercises, we will resume at the agreed time rather than wait for people to rejoin. This is important. 

Terms of reference exercise (see fundamentals course). Have them come up with their own list. Such as punctuality (can also reference timeboxing), use of mobile phones in class, emails etc. 

Guilty pleasures list. Invite them to contribute a title to the track list. Tunes to be played during breaks and breakouts. Hang on the wall.

---

# Activities for Introduction

## 1. Welcome
  - Welcome, domestics, AFA/virtual issues, icAgile background

  Emphasise that this isn’t a programming course.
  We are not going to help them with syntax or debug etc. That’s on them.

*Time box:* **20 minutes**

## 2. Introductions
  - Pair learners,have them interview one another, why are they here? , what do they want to know? (See <a href="./00_1_Introductions.md">Introductions (file 00_1_Introductions.md)</a>)

*Time box:* **30 minutes**

## 3. Set Up

*This is going to be the activity for Version Control*

See the guide called <a href="./00_2_SetUp.md">Set Up (file ./00_2_SetYp.md)</a> for learner set up instructions.

*Time box:* **20 minutes**

### Notes:

* Provide AFA learners with LOD links
* Assign all to pairs
* Test out the webex team feature to go off and work privately.  Include desktop share

---

# Trainer Observations

Please list any observations or updates that you have from this section.

* First observation...(replace the text but not the *)