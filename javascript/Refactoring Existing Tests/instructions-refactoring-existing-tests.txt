instructions-refactoring-existing-tests.txt

Refactoring existing tests exercise

Work in pairs.

Continue working on the Calculator project.

Robert Martin says good tests are more valuable than production code

Tests are also code, tests may introduce complexity

Identify any flaws in tests and chose appropriate refactoring options
and apply to make the tests more readable and easy to update.

You may consider renaming, moving code around and extracting methods

You may consider the following design steps for testing

1) setting up fixtures (creating objects) Ex: setUp() with @Before
2) creating test case methods
3) Writing assertions (one assert per test case method)
4) Writing cleanup method (tearDown() with @After annotation)



Remember to keep your repository updated!

